//
//  StrainerProvider+Utils.swift
//  Strainer
//
//  Created by Tristan Leblanc on 27/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import Foundation

/// invertedIndex
///
/// invertedIndex returns a dictionary [object key : object index]
/// It is used to determine object indexes quickly

extension Array where Element : KeyedObject {
	
	var invertedIndex: [ObjectKey: Int] {
		var invertedIndex = [ObjectKey: Int]()
		var idx = 0
		for item in self {
			invertedIndex[item.key] = idx
			idx += 1
		}
		return invertedIndex
	}
}

public extension Strainer {
	
	/// Strainer.Changes
	///
	/// after one or more filter as been re-applied, the provider returns the changes in this struct
	
	public struct Changes {
		public var addedRows: [Int]
		public var removedRows: [Int]
		public var movedRows: [(Int,Int)]
		public var unchangedRows: [Int]
		
		static let noChanges = Changes(addedRows: [Int](), removedRows: [Int](), movedRows: [(Int, Int)](), unchangedRows: [Int]())
		
		public var description: String {
			return "----> Table changes:\rRows to remove : \(removedRows.count)\rRows to add : \(addedRows.count)\rRows to move : \(movedRows.count)\rUnchanged rows ( to reload) : \(unchangedRows.count)"
		}
	}
	
}

extension Strainer.Provider {
	
	func determineAddedRemovedIndexes(previousItems: [T], newItems: [T]) -> Strainer.Changes {
		
		var indexesToAdd = [Int]()
		var indexesToRemove = [Int]()
		var movedIndexes = [(Int,Int)]()
		var unchangedIndexes = [Int]()
		
		let date = Date()
		
		if newItems.isEmpty {
			if previousItems.isEmpty {
				return Strainer.Changes.noChanges
			}
			for i in 0 ... previousItems.count-1 {
				indexesToRemove.append(i)
			}
		}
		else if previousItems.isEmpty {
			for i in 0 ... newItems.count-1 {
				indexesToAdd.append(i)
			}
		}
		else {
			
			if previousItems.count >= newItems.count {
				var invertedIndex = newItems.invertedIndex
				
				var pi = 0
				for p in previousItems {
					if let ni = invertedIndex[p.key] {
						if pi == ni {
							unchangedIndexes.append(pi)
						} else {
							movedIndexes.append((pi,ni))
						}
					}
					else {
						indexesToRemove.append(pi)
					}
					invertedIndex.removeValue(forKey: p.key)
					pi += 1
				}
				
				for (_,value) in invertedIndex {
					indexesToAdd.append(value)
				}
			}
			else {
				var invertedIndex = previousItems.invertedIndex
				
				var ni = 0
				for n in newItems {
					if let pi = invertedIndex[n.key] {
						if pi == ni {
							unchangedIndexes.append(pi)
						} else {
							movedIndexes.append((pi,ni))
						}
					}
					else {
						indexesToAdd.append(ni)
					}
					invertedIndex.removeValue(forKey: n.key)
					ni += 1
				}
				
				for (_,value) in invertedIndex {
					indexesToRemove.append(value)
				}
			}
			
		}
		let t2 = -date.timeIntervalSinceNow * 1000
		print("Items determined time : \(t2 )")
		return Strainer.Changes(addedRows: indexesToAdd, removedRows: indexesToRemove, movedRows: movedIndexes, unchangedRows: unchangedIndexes)
	}
	
	public var debugIdentifier: String {
		return identifier ?? "\(self)"
	}
	
	public var debugPredicate: String {
		return "NULL"
	}
	
	public var debugDescription: String {
		
		var str = "Object Provider \(debugIdentifier) - number of objects: \(count)"
		var pad = "  "
		enumerateFilters() { filter in
			str += "\r\(pad) -> Filter : \(filter.debugIdentifier) - Active: \(filter.active) - Predicate : \(debugPredicate) - storedObjects.count = \(filter.storedObjects?.count ?? -1) -  objects.count = \(filter.objects.count ) - number of objects: \(filter.count)"
			pad += "  "
		}
		return str
	}
	
}

