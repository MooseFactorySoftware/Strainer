//
//  StrainerProvider.swift
//  Strainer Framework
//
//  Created by Tristan Leblanc on 25/05/2018.
//  Copyright © 2018 MooseFactory. All rights reserved.
//

import Foundation


public struct Strainer {
	
	fileprivate init() {}
	
	open class Provider<T : KeyedObject> {
		
		let processQueue = DispatchQueue(label: "StrainerQueue", qos: .background, attributes: .concurrent)
		
		public var storedObjects: [T]? { didSet {
			objects = storedObjects ?? [T]()
			}}
		
		public var objects: [T] = [T]()
		
		public var processingObjects: [T] {
			let lastFilter = self.lastFilter
			return lastFilter?.processingObjects ?? storedObjects ?? [T]()
		}
		
		public var lastFilter: Filter<T>? {
			var f = self.outFilter
			while f?.outFilter != nil {
				f = f?.outFilter!
			}
			return f
		}
		
		public var identifier: String?
		
		/// outFilter
		///
		/// If a filter is attached, the input of the filter is set to self, and the filter wil  be marked as changed
		
		public var outFilter: Filter<T>?
		
		public var first: T? { return objects.first }
		
		public var last: T? { return objects.last }
		
		public var count: Int { return objects.count }
		
		public var locked = false
		
		var filterWorkItem: DispatchWorkItem?
		
		//MARK: - Life cycle
		
		public init(identifier: String? = nil, objects: [T]? = nil) {
			self.identifier = identifier
			self.storedObjects = objects
			self.objects = objects ?? [T]()
		}
		
		// Access objects
		
		public func object(at index: Int) -> T? {
			guard index >= 0, index < objects.count else {
				return nil
			}
			return objects[index]
		}
		
		public subscript(index: Int) -> T? {
			return object(at: index)
		}
		
		public func object(with key: ObjectKey) -> T? {
			return objects.filter{$0.key == $0.key}.first
		}
		
		
		subscript(key: ObjectKey) -> T? {
			return object(with: key)
		}
		
		// Get objects indexes
		
		public func index(of object: T) -> Int? {
			return objects.index(where: { (obj) -> Bool in
				return obj.key==object.key
			})
		}
		
		// Enumerate filters
		
		public func enumerateFilters(function: (Filter<T>)->Void) {
			var filter = outFilter
			while filter != nil {
				function(filter!)
				filter = filter!.outFilter
			}
		}
		
		public func resetFilters() {
			enumerateFilters { (filter) in
				filter.storedObjects = nil
			}
			outFilter?.changed = true
		}
		
		public func refresh(reset: Bool = false, completion: @escaping (Changes) -> Void) {
			if locked { return }
			//TODO: - Temporary - Won't work in case of provider objects are changed
			guard let _ = self.outFilter else {
				return completion(Changes.noChanges)
			}
			
			if filterWorkItem != nil {
				filterWorkItem?.cancel()
				self.filterWorkItem = nil
			}
			
			var changes: Changes?
			
			self.filterWorkItem = DispatchWorkItem(qos: .userInitiated, flags: DispatchWorkItemFlags.inheritQoS) {
				
				if reset {
					self.resetFilters()
				}
				let previousObject = self.processingObjects
				
				self.enumerateFilters { (filter) in
					if filter.changed {
						filter.apply()
					}
				}
				
				let newObjects = self.processingObjects
				
				changes = self.determineAddedRemovedIndexes(previousItems: previousObject, newItems: newObjects)
				
				#if DEBUG
				print("\(self.debugDescription)")
				#endif
				
			}
			self.filterWorkItem?.notify(queue: DispatchQueue.main) {
				if let _changes = changes, let isCancelled = self.filterWorkItem?.isCancelled, !isCancelled {
					self.commit()
					completion(_changes)
				}
				self.filterWorkItem = nil
			}
			
			processQueue.sync(execute: self.filterWorkItem!)
			
		}
		
		func commit() {
			self.objects = processingObjects
		}
	}
	
}
