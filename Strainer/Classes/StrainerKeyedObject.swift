//
//  KeyedObject.swift
//  Strainer Framework
//
//  Created by Tristan Leblanc on 26/05/2018.
//  Copyright © 2018 MooseFactory. All rights reserved.
//

import Foundation

public protocol KeyedObject {
	var key: ObjectKey { get }
}

public enum ObjectKey : Equatable, Hashable {
	case int(Int)
	case string(String)
	
	public var int:Int {
		switch(self)
		{
		case .int(let intValue): return intValue
		case .string(let stringValue): return NSString(string: stringValue).integerValue
		}
	}
	
	public var string:String {
		switch(self)
		{
		case .int(let intValue): return "\(intValue)"
		case .string(let stringValue): return stringValue
		}
	}
	
	public static func == (lhs: ObjectKey, rhs: ObjectKey) -> Bool {
		switch(lhs)
		{
		case .int: return lhs.int == rhs.int
		case .string: return lhs.string == rhs.string
		}
	}
	
	public var hashValue: Int {
		switch(self)
		{
		case .int: return self.int.hashValue
		case .string: return self.string.hashValue
		}
	}
}
