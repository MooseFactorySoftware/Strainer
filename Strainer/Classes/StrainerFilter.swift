//
//  Strainer.Filter.swift
//  Strainer Framework
//
//  Created by Tristan Leblanc on 27/05/2018.
//  Copyright © 2018 MooseFactory. All rights reserved.
//

import Foundation


extension Strainer {
	
	public struct Functions {
		public typealias Sort<T: KeyedObject> = ([T],[String:Any]?) -> [T]
		public typealias Filter<T: KeyedObject> = ([T],[String:Any]?) -> [T]?
	}
	
	public enum FunctionType {
		case sort
		case filter
	}
	
	public enum Function<T: KeyedObject> {
		case sortFunction(Functions.Sort<T>)
		case filterFunction(Functions.Filter<T>)
		
		var type: FunctionType {
			switch self {
			case .sortFunction:
				return .sort
			case .filterFunction:
				return .filter
			}
		}
	}
	
	/// Strainer.Filter
	///
	/// A Strainer.Filter is a special provider.
	/// If storedObjects ( cache ) is nil, then it gets is objects from the input provider
	/// If the filter is applied and the result is different from the input, then it stores the results in storedObjects
	///
	/// This let the filter returns objects without recomputation
	///
	/// If property susceptible to change the results is changed, then the changed property is set to true.
	/// This way, when apply is called on the source provider, only changed filters reprocess their results
	
	open class Filter<T : KeyedObject> : Provider<T> {
		
		/// input
		///
		/// the input provider
		
		public var input: Provider<T>? { didSet {
			if input == nil {
				storedObjects = nil
			}
			changed = true
			input?.outFilter?.input = nil // unplug previous filter
			input?.outFilter = self
			}}
		
		/*
		
		NOT USED YET
		
		public var predicate: NSPredicate? { didSet {
		changed = true
		}}
		
		*/
		
		public var function: Function<T>
		
		public var context: [String: Any]?
		
		/// changed
		///
		/// indicates the stored objects in this filter may not be valid anymore
		/// value is propagated to all following filters if true
		
		public var changed: Bool = true { didSet {
			if !changed { return }
			enumerateFilters { filter in
				filter.changed = true
			}
			}}
		
		/// shouldClearCache
		///
		/// indicates the stored objects should be set to nil before the filter is processed.
		///
		/// A common example is the case of the text search
		/// If some characters are appended to the searchstring, then the filter can reduce the current stored objects set
		/// If some characters are deleted, then it clears the cache and recompute filtered object from input
		
		public var shouldClearCache: Bool = true
		
		/// active
		///
		/// if a filter is inactive, it become a pass-through and has no impact on the objects
		
		public var active: Bool = true { didSet {
			changed = true
			}}
		
		/// _active
		///
		/// the effective active state
		/// when active is changed, then the filter waits for the next process to set it internally
		/// this way we can compute the differences
		//TODO: - ?? really necessary ??
		
		var _active: Bool = true
		
		/// objects
		/// returns the output obbject
		
		override public var processingObjects: [T] {
			
			if !_active || storedObjects == nil {
				// if input is a filter, continue to go down the chain to previous filter
				if let input = self.input as? Filter {
					return input.processingObjects
				}
				// return source objects
				return input?.storedObjects ?? [T]()
			}
			return storedObjects ?? [T]()
		}
		
		public required init(input: Provider<T>, function: Function<T>) {
			self.function = function
			self.input = input
			// DidSet is not called at this time so we set the output of the input ;)
			super.init()
			input.outFilter = self
		}
		
		/// apply
		///
		/// filters the stored objects if clearCache is false
		/// filters the input obbjects if clearCache is true
		/// propagates to following filters
		
		public func apply() {
			if !changed {
				outFilter?.apply()
				return
			}
			print("Will apply filter \(debugIdentifier) - active: \(active) - will clear cache : \(shouldClearCache)")
			// set active flag for real
			_active = active
			
			if shouldClearCache {
				storedObjects = nil
			}
			
			if let input = self.input {
				switch function {
				case .sortFunction(let function):
					self.storedObjects = function(input.processingObjects,context)
				case .filterFunction(let function):
					self.storedObjects = function(input.processingObjects,context)
				}
				
				changed = false
				
				outFilter?.apply()
			}
			
		}
	}
	
	public static let SearchStringKey: String = "searchString"

	open class TextSearchFilter<T : KeyedObject>  : Filter<T> {
		

	public var searchString: String? { didSet {
		guard let string = searchString, !string.isEmpty else {
			shouldClearCache = true
			active = false
			changed = true
			return
		}
		
		let prevLength = (oldValue ?? "").count
		let newLength = string.count
		shouldClearCache = newLength<prevLength
		active = true
		changed = true
		context = [Strainer.SearchStringKey: string.lowercased()]
		}}
	}
}
