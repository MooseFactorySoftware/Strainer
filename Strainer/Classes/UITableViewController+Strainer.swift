//
//  UITableViewController+Strainer.swift
//  Strainer
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import UIKit

public extension UITableViewController {
	
	public func updateChanges(_ changes: Strainer.Changes, completion:(()->Void)? = nil) {
		
		if #available(iOS 11.0, *) {
			self.tableView.performBatchUpdates({
				self.executeUpdateChanges(changes)
			}) { _ in
				completion?()
			}
		} else {
			tableView.beginUpdates()
			self.executeUpdateChanges(changes)
			tableView.endUpdates()
			completion?()
		}
	}
	
	func executeUpdateChanges(_ changes: Strainer.Changes) {
		let added = changes.addedRows.map{IndexPath(row: $0, section: 0)}
		let removed = changes.removedRows.map{IndexPath(row: $0, section: 0)}
		let unchanged = changes.unchangedRows.map{IndexPath(row: $0, section: 0)}
		let moved = changes.movedRows.map{(IndexPath(row: $0.0, section: 0), IndexPath(row: $0.1, section: 0))}
		
		tableView.deleteRows(at: removed, with: UITableViewRowAnimation.automatic)
		tableView.insertRows(at: added, with: UITableViewRowAnimation.automatic)
		tableView.reloadRows(at: unchanged, with: UITableViewRowAnimation.none)
		if added.isEmpty && removed.isEmpty {
			for move in moved {
				tableView.moveRow(at: move.0, to: move.1)
			}
		} else {
			tableView.reloadRows(at: moved.map{$0.0}, with: UITableViewRowAnimation.none)
		}
	}
	
}
