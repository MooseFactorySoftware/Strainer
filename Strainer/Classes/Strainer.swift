//
//  Strainer.swift
//  Strainer
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import Foundation

/// StrainerProviderProtocol
///
/// Any custom provider should conform to this protocol to be compatible with Strainer

public protocol StrainerProviderProtocol {
	
	associatedtype T: KeyedObject
	
	init(identifier: String?, objects: [T]?)
	
	/// storedObjects
	///
	/// contains cached objects. If there is no filter, calling objects will returns storedObjects
	
	var storedObjects: [T]? { get set }
	
	/// objects
	///
	/// get-only - objects returns the filtered objects
	/// it returns the objects at the end of the chain for a source provider
	/// it returns the filtered objects for a filter
	
	
	var processingObjects: [T] { get }
	
	/// count
	///
	/// get-only - Return the number of objects in the objects property
	
	var count: Int { get }
	
	/// outFilter
	///
	/// return the filter plugged on this provider
	
	var outFilter: Strainer.Filter<T>? { get }
	
	/// lastFilter
	///
	/// return the filter at the end of the chain
	
	var lastFilter: Strainer.Filter<T>? { get }
	
	/// first
	///
	/// return the first object if it exists
	
	var first: T? { get }
	
	/// last
	///
	/// return the last object if it exists
	
	var last: T? { get }
	
	/// object(at: Int)
	///
	/// return the object at index or nil if index is out of range
	
	func object(at: Int) -> T?
	
	// [Index] subscript version of object(at:)
	subscript(index: Int) -> T? { get }
	
	/// object(with: key)
	///
	/// return the object with given key value if it exists
	
	func object(with key: ObjectKey) -> T?
	
	// [Key] subscript version of object(with:)
	subscript(key: ObjectKey) -> T? { get }
	
}
