![StrainerLogo](Documentation/StrainerLogo.jpg)

# Strainer

**Seamless objects sorting, filtering and table updates on iOS**

<https://gitlab.com/MooseFactorySoftware/Strainer>

![StrainerVideo](https://j.gifs.com/0VjXQL.gif)

# Usage

## Add Strainer to a table view controller

See how you can create a provider, and append chained filters to sort values and search by various parameters

The examples shows a countries table. You can see the whole Strainer Lab application source here:

<https://gitlab.com/MooseFactorySoftware/StrainerLab>

### Add an objects provider
	
``` swift

#import UIKit
#import Strainer

class countriesTableViewController {

	/// countries
	///
	/// The countries objects provider.

	lazy var countries: Strainer.Provider<Country> = {
		let provider = Strainer.Provider<Country>(objects: self.countriesData)
		return provider
	}()
	
}
```

### Bind to the table

``` swift
extension CountriesTableViewController {
	
    override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return countries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    	let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath)
		if let country = countries[indexPath.row] {
			cell.textLabel?.text = country.name
			cell.detailTextLabel?.text = country.code
		 }
        return cell
    }
}
```

That's it.. You may think "And so what?"
So let's see how to add a sorting filter

## Add a sorting filter


### Define the filter

**Strainer** defines filter foundation, but you are responsible of implementing the filtering function.
This avoid too much asumptiuns on the model and let you complete freedom on the way you sort your objects

In the case of a sort, we implement a ```Strainer.Function.sortFunction```


``` swift
extension CountriesTableViewController {

	class Sorter : Strainer.Filter<Country> {
				
		static let sortByName = Strainer.Function<Country>.sortFunction({ (items, _) -> [Country] in
			return items.sorted(by: { (lhl,lhr) in lhl.name < lhr.name })
		})
		
	}
}
```

### Use the filter
 
#### Init a Sorter filter in our view controller

``` swift
/// sortByNameFilter
///
/// The filter that sorts countries by name.
	
lazy var sortByNameFilter: Sorter = {
	Sorter(input: countries, function: Sorter.sortByName)
}()
```
	
#### Apply the filter and updates the results

``` swift
override func viewDidLoad() {
	sortByNameFilter.active = true
	countries.refresh(completion: { _ in
		self.tableView.reloadData()
	})
}
```

This is a trivial exemple, and it is still possible to say "and so what".<br>But considering you can chain filters, activate/desactive , share them among table view controllers, it starts to get sense.

Let's go a bit further

## Add a search filter

### Define the search filter

In the case of a filtering, we implement a ```Strainer.Function.filterFunction```

``` swift
class SearchFilter : Strainer.TextSearchFilter<Country> {
	
	static let containsString = Strainer.Function<Country>.filterFunction({ (items, context) -> [Country]? in
		guard let string = context?[Strainer.SearchStringKey] as? String else {
			return nil
		}
		return items.filter{ $0.name.lowercased().contains(string) || $0.code.lowercased().contains(string) }
	})
	
}
```

### Use the search filter
 
#### Init a TextSearchFilter in our view controller

We init the search filter with our previous filter as input

``` swift
lazy var searchFilter: SearchFilter = {
	SearchFilter(input: scopeFilter, function: SearchFilter.containsString)
}()
```
	
#### Apply the filter and updates the results

``` swift
	override func viewDidLoad() {
		sortByNameFilter.active = true
		searchFilter.searchString = nil
		countries.refresh(completion: { _ in
			self.tableView.reloadData()
		})
	}
```

This is how the table is updated after the search string is changed by the user.
Note that **Strainer** takes care of the reload of the table, with the proper animations.

The **changes** structure tells how much rows are unchanged, have been added, removed or moved

``` swift
extension CountriesTableViewController : UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		searchFilter.searchString = searchText
		countries.refresh(completion: { changes in
			self.updateChanges(changes)
		})
	}
}
```




***

# Installation

**Strainer** is available through CocoaPods. To install it, simply add the following line to your Podfile:

	pod 'Strainer'

# Author

Tristan Leblanc <tristan@moosefactory.eu>

Twitter     :	<https://www.twitter.com/tristanleblanc>  
Google+     :	<https://plus.google.com/+TristanLeblanc>  

***

# Contribute

If you wish to contribute, check the [CONTRIBUTE](CONTRIBUTE.md) file for more information.

***

# License

**Strainer** is available under the MIT license. See the [LICENSE](LICENSE) file for more info.

***

*Preliminary document - 31 May 2018*
