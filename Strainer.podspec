#
# Be sure to run `pod lib lint Strainer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

  s.name             = "Strainer"
  s.version          = "0.1.1"
  s.swift_version	 = "4.1"
  s.summary          = "Seamless objects sorting, filtering and table updates"
  s.description      = <<-DESC
							Strainer let you add an objects provider that can be linked with any sort or search filters.
							Filters can be chained, and filters parameters changes can be seamlessly reflected on the interface.
                       DESC
  s.homepage         = "https://gitlab.com/MooseFactorySoftware/Strainer"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Tristan Leblanc" => "tristan@moosefactory.eu" }
  s.source           = { :git => "https://gitlab.com/MooseFactorySoftware/Strainer", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/moosefactory_eu'

	s.platform = 'ios'
    s.ios.deployment_target = '10.0'
    s.requires_arc = true

	s.source_files = 'Strainer.h', 'Strainer/Classes/*'

end
